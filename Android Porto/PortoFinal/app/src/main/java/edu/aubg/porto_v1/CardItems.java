package edu.aubg.porto_v1;

/**
 * Created by amako on 2/20/2018.
 */

public class CardItems {

    int imageSourse;

    String text;


    public CardItems(int imageSourse, String text) {
        this.imageSourse = imageSourse;
        this.text = text;
    }

    public int  getImageSourse() {
        return imageSourse;
    }

    public String getText() {
        return text;
    }
}
