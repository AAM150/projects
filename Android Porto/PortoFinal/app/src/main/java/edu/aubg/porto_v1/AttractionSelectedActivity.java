package edu.aubg.porto_v1;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import java.net.URI;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by amako on 2/21/2018.
 */

public class AttractionSelectedActivity extends Activity {

    VideoView video;
    Switch play;
Button btn;

TextView text;
SpeechRecognizer recognizer;
Intent  speechRecognizerIntent;

String comand;

private Context ctx;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selectedattractionview);

        FragmentManager menu= getFragmentManager();
        FragmentTransaction selectedAttractionFragment=menu.beginTransaction();

      //  selectedAttractionFragment.replace(R.id.floadmenuFromComand,new SelectedItemFragment()).commit();

        Bundle attraction = getIntent().getExtras();
   //     String commnad= attraction.getString("attraction");
        SelectedItemFragment selectedItem=new SelectedItemFragment();
        selectedItem.setArguments(attraction);
        selectedAttractionFragment.replace(R.id.floadmenuFromComand,selectedItem).commit();



       // checkPermission();

        /*recognizer= SpeechRecognizer.createSpeechRecognizer(this);

        speechRecognizerIntent=new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH);
*/
      //  text=(TextView)findViewById(R.id.textView);
/*
        recognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle bundle) {

            }

            @Override
            public void onBeginningOfSpeech() {

            }

            @Override
            public void onRmsChanged(float v) {

            }

            @Override
            public void onBufferReceived(byte[] bytes) {

            }

            @Override
            public void onEndOfSpeech() {

            }

            @Override
            public void onError(int i) {

            }

            @Override
            public void onResults(Bundle bundle) {

                ArrayList<String> matches=bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                if(matches!=null)
                {

                    comand=matches.get(0);

                    text.setText(comand);
                    SetComandAction(comand,ctx);

                }
            }

            @Override
            public void onPartialResults(Bundle bundle) {

            }

            @Override
            public void onEvent(int i, Bundle bundle) {

            }
        });*/

      // ctx= getApplicationContext();

       // video=(VideoView) findViewById(R.id.videoView2);

       // play=(Switch)findViewById(R.id.switch1);

      //  btn=(Button)findViewById(R.id.button);
       /** play.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                      String path="android.resource://"+getPackageName()+"/"+R.raw.portointro;

                        Uri uri=Uri.parse(path);
                        video.setVideoURI(uri);
                        if(b)
                        {
recognizer.stopListening();
                        //video.start();
                        }
                        else video.pause();

                    }
                }
        );


        btn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        record(recognizer,speechRecognizerIntent);
                    }
                }
        );
        */



    }



  /*  public void record(SpeechRecognizer rec,Intent i){


        rec.startListening(i);




    }*/

/*public void checkPermission(){

        if(!(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)== PackageManager.PERMISSION_GRANTED ))
        {
            Intent intent=new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.parse("package:"+getPackageName()));

            startActivity(intent);
            finish();
        }
}
*/

/*public void SetComandAction(String s,Context context)
{
    Intent f = new Intent();


    switch (s)
    {
        case"explore":

            f.setClass(context,Welcome.class);

            startActivity(f);
        break;

        case"menu":


            FragmentManager menu= getFragmentManager();
            FragmentTransaction menut=menu.beginTransaction();

            menut.replace(R.id.floadmenuFromComand,new menufragment()).commit();



            break;
            default:
                text.setText("No comand given ");



    }




}
*/

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_welcome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
