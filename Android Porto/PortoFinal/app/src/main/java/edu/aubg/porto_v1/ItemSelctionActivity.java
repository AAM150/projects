package edu.aubg.porto_v1;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static android.app.PendingIntent.getActivity;

public class ItemSelctionActivity extends AppCompatActivity {
private RecyclerView recView;
private RecyclerView.Adapter cardadapter;
private List<CardItems> items;
;
public CardView cardView;
VoiceContorl voiceContorl;
/////// BEGIN ACTIVITY
    private static Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_item_selction);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
////////////REAL CODE STARTS HERE////////////////////


        //////FIND THE RECYCLE VIEW WHICH IS DEFINED IN ITS SEPARATE XML
        recView=(RecyclerView)findViewById(R.id.recyclerView1);


        /////sETTING THE LAYOUT MANAGER

        recView.setLayoutManager(new LinearLayoutManager(this));

        //BUNDLE GETS THE EXTRA MESSAGE PASSEG FROM THE MENUFRAGMENT
        Bundle cardInfo=getIntent().getExtras();
        String message=cardInfo.getString("cardid");
////////////////////////////////////////////////////

        items=new ArrayList<>();////DECALARE LIST THAT WILL BE FEED TO THE ADAPTER FOR THE DISPLAY


        if(message.equals("LandMarks"))
            items=LoadLandmarks();////IF THE MENUFRAGMENT PASSED  LANDMARKS THE LOAD LANDMARKS IS CALLED


        if(message.equals("HistoricSites"))///IF THE IF THE MENUFRAGMENT PASSED  HISTORICSITES THE LOAD HISTORICSITES IS CALLED
            items=LoadHistoricalSites();


        if(message.equals("Wine"))
            items=LoadWine();

        cardadapter=new CardItemAdapter(items,this);
        recView.setAdapter(cardadapter);

        ////////////////////////////////////////delete all this if things break ////////////////////////////////

        ////////////////////////////////////////////////////////////////////////////////////////////////
     //   recView.findViewHolderForLayoutPosition();











    }



    public List LoadLandmarks() {

///////////GET THE ID OF ALL THE PICS FOR THE LAND MARK
        int stadium = R.mipmap.estadiodragao;

        int library = R.mipmap.livrarialelo;

        int bridge = R.mipmap.bridgeclimb;
        ///////////////////////////////////////////////////////////


///////////////DECLARE OBJECT LIST
        ////POPULATE OBJECT LIST
        List<CardItems> landmarks = new ArrayList<>();

        landmarks.add(new CardItems(stadium, "Estadio Dragao"));
        landmarks.add(new CardItems(library, "Livraria Lelo"));

        landmarks.add(new CardItems(bridge, "Porto Bridge"));
/////////////////////////////////////////////////////


        return landmarks ////RETURN BACK THE OBJECT LIST TO THE LIST THAT WILL BE FED TO THE ADEPTER
                ;
    }

        public List LoadHistoricalSites()

        {

        int pedro=R.mipmap.donpedroporto;

        int cathedral=R.mipmap.portocathedral;

        int barbosa=R.mipmap.portohistoricsites;


            List<CardItems> historical_sites = new ArrayList<>();
            historical_sites.add(new CardItems(pedro, "Don Pedro"));
            historical_sites.add(new CardItems(cathedral, "Porto Cathedral"));

            historical_sites.add(new CardItems(barbosa, "Palacio da Bolsa"));


            return historical_sites;
       }




      public List LoadWine()
       {


           int cellar= R.mipmap.cellar;

           int fereirra= R.mipmap.wineferreira;

           int sandeman =R.mipmap.sandeman;

           List<CardItems> wines = new ArrayList<>();
           wines.add(new CardItems(cellar, "Croft"));
           wines.add(new CardItems(fereirra, "Ferreira"));

           wines.add(new CardItems(sandeman, "Sandeman"));


           return wines;






       }

    }




