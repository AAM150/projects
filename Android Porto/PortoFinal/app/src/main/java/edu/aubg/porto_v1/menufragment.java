package edu.aubg.porto_v1;

import android.app.Fragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by amako on 2/19/2018.
 */

public class menufragment extends Fragment {


    CardView card,card2,card3;
String command;
    ImageButton btn;
    VoiceContorl vc;
    TextView text;
    private  static Context cxt;
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragmentmenu,container,false  );




        card=(CardView) view.findViewById(R.id.card1);//// getting the landmarks card
        card2=(CardView) view.findViewById(R.id.HistoricSites);// getting the historic sites card
        card3=(CardView)view.findViewById(R.id.FoodandWine);//getting the food an wine card
        //////set up listeners for every card ///////////////
        /////////////////////////////////setting up voice cotnroll for item selection

        cxt=getActivity().getApplicationContext();
        vc=new VoiceContorl(cxt);

        btn=(ImageButton) view.findViewById(R.id.button2);

        btn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        vc.record(vc.getRecognizer(),vc.getIntent());

                    }
                }
        );



        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        card.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        loadCard(view);
                    }
                }////loads the landmarks menu
        );

        card2.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        loadHistoricsites(view);
                    }
                }////uppon click loads the historic sites menu
        );


        card3.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        loadWine(view);
                    }
                }//upon click loads the food an wine menu
        );
        return  view;
    }


    public void loadCard(View view)///load the Landmarks///
 {
        ////initialize intent to switch to second activity //
        //pass along extra information to the Item selection activity ////
        Intent itemSelect=new Intent();
        itemSelect.setClass(getActivity(),ItemSelctionActivity.class);
        itemSelect.putExtra("cardid","LandMarks");///extra information = Lanrmarks
        getActivity().startActivity(itemSelect);


    }


    public void loadHistoricsites(View view) {

        //load the Historic sites activity
        ////intent to switch to the Item selection activity
        ///pass extra information
        Intent itemSelect=new Intent();
        itemSelect.setClass(getActivity(),ItemSelctionActivity.class);
        itemSelect.putExtra("cardid","HistoricSites");
        getActivity().startActivity(itemSelect);
    }

    public void loadWine(View view)
    {
        //load the FoodandWine sites activity
        ////intent to switch to the Item selection activity
        ///pass extra information

        Intent itemSelect=new Intent();
        itemSelect.setClass(getActivity(),ItemSelctionActivity.class);
        itemSelect.putExtra("cardid","Wine");
        getActivity().startActivity(itemSelect);
    }

}
