package edu.aubg.porto_v1;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by amako on 2/20/2018.
 */

public class CardItemAdapter extends RecyclerView.Adapter<CardItemAdapter.ViewHolder>{

    public List<CardItems> cardslist;
    public Context context;

    public CardView card;
    public CardItemAdapter(List<CardItems> cards, Context context) {
        this.cardslist = cards;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
     View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview,parent,false);

        card=(CardView) view.findViewById(R.id.MenuElemntCard);


        card.setOnClickListener(new View.OnClickListener() {/////SET UP EVENT LISTENER FOR WHEN THE CARD IS CLICKED
            @Override
            public void onClick(View v) {





                //Toast toast = Toast.makeText(context, currentCard.getImageSourse(), Toast.LENGTH_SHORT );
                //toast.show();
            }
        });


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

   final CardItems carditem=cardslist.get(position);


   holder.image.setImageResource(carditem.getImageSourse());


   holder.text.setText(carditem.getText());

   holder.layout.setOnClickListener(
           new View.OnClickListener() {
               @Override
               public void onClick(View view) {
                   Intent selectedAttractio=new Intent();

                   selectedAttractio.setClass(context,AttractionSelectedActivity.class);
                   selectedAttractio.putExtra("attraction",carditem.getText());

                   context.startActivity(selectedAttractio);
               }
           }
   );

    }

    @Override
    public int getItemCount() {
        return cardslist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
public ImageView image;
public TextView text;
public RelativeLayout layout;

        public ViewHolder(View itemView) {
            super(itemView);


           image=(ImageView) itemView.findViewById(R.id.ElementPicture);

           layout= (RelativeLayout) itemView.findViewById(R.id.CardItemId);

           text=(TextView)itemView.findViewById(R.id.ElementText);
        }
    }





}
