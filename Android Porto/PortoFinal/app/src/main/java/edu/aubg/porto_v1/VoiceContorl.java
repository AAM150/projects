package edu.aubg.porto_v1;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by amako on 2/22/2018.
 */

public class VoiceContorl extends Activity {
  private  SpeechRecognizer recognizer;
  private   Intent speechRecognizerIntent;

    public String comand;

    //private static Context voiceContext;


    public VoiceContorl(final Context c)
    {
      //  voiceContext= getApplicationContext();

      //  checkPermission();
        recognizer= SpeechRecognizer.createSpeechRecognizer(c);

        speechRecognizerIntent=new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH);



        recognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle bundle) {

            }

            @Override
            public void onBeginningOfSpeech() {

            }

            @Override
            public void onRmsChanged(float v) {

            }

            @Override
            public void onBufferReceived(byte[] bytes) {

            }

            @Override
            public void onEndOfSpeech() {

            }

            @Override
            public void onError(int i) {

            }

            @Override
            public void onResults(Bundle bundle) {

                ArrayList<String> matches=bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                if(matches!=null)
                {

                    comand=matches.get(0);

                   // txt.setText(comand);
                    SetComandAction(comand,c);

                }
                else comand="i am null ";
            }

            @Override
            public void onPartialResults(Bundle bundle) {

            }

            @Override
            public void onEvent(int i, Bundle bundle) {

            }
        });


    }


    /////start here

    public String getCommand()
    {


        return comand;
    }


public SpeechRecognizer getRecognizer()
{

    return  recognizer;

}

public Intent getIntent()
{


    return speechRecognizerIntent;
}


    public void record(SpeechRecognizer rec,Intent i){


        rec.startListening(i);




    }




    public void SetComandAction(String s,Context context)
    {
        Intent f = new Intent();


        switch (s)
        {
            case"explore":

                f.setClass(context,Welcome.class);

               context.startActivity(f);
                break;

            case"menu":


                FragmentManager menu= getFragmentManager();
                FragmentTransaction menut=menu.beginTransaction();

                menut.replace(R.id.floadmenuFromComand,new menufragment()).commit();



                break;



            case"landmark":
                Intent itemSelect=new Intent();
                itemSelect.setClass(context,ItemSelctionActivity.class);
                itemSelect.putExtra("cardid","LandMarks");///extra information = Lanrmarks
                context.startActivity(itemSelect);
                break;
            case"historic":
                Intent historic=new Intent();
                historic.setClass(context,ItemSelctionActivity.class);
                historic.putExtra("cardid","HistoricSites");///extra information = Lanrmarks
                context.startActivity(historic);
                break;
            case"wine":
                Intent wine=new Intent();
                wine.setClass(context,ItemSelctionActivity.class);
                wine.putExtra("cardid","Wine");///extra information = Lanrmarks
                context.startActivity(wine);
                break;
                //////////////////////////////////////////////delete all this if things break

            default:
             break;



        }




    }





    public void checkPermission(){

        if(!(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)== PackageManager.PERMISSION_GRANTED ))
        {
            Intent intent=new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:"+getPackageName()));

            startActivity(intent);
            finish();
        }
    }












}
