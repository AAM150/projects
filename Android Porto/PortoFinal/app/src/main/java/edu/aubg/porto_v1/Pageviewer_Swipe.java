package edu.aubg.porto_v1;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

/**
 * Created by amako on 2/27/2018.
 */

public class Pageviewer_Swipe extends PagerAdapter {

    private List<Integer>content_pictures;
    private Context ctx;
    private LayoutInflater layoutInflater;

    public Pageviewer_Swipe(List<Integer> content_pictures, Context ctx) {
        this.content_pictures = content_pictures;
        this.ctx = ctx;
    }

    @Override
    public int getCount() {
        return content_pictures.size();
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView= new ImageView(ctx);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageResource(content_pictures.get(position));
        container.addView(imageView,0);
        return imageView;
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ImageView)object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }
}
