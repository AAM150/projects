package edu.aubg.porto_v1;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.VideoView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * Created by amako on 2/22/2018.
 */

public class SelectedItemFragment extends Fragment {

    SpeechRecognizer recognizer;
    Intent  speechRecognizerIntent;
    TextView txt;
List<Integer>stuff;
    public static int position;
    VideoView video;
    Switch play;
    ImageButton btn;
  public static   String path;
    public String comand;
   private static   Context cntex;
   RelativeLayout layout;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.selecteditemfragmentview, container, false);

        cntex=getActivity().getApplicationContext();
        checkPermission();


        txt=(TextView)view.findViewById(R.id.textView);

        recognizer= SpeechRecognizer.createSpeechRecognizer(cntex);

        speechRecognizerIntent=new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

        speechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH);


        recognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onReadyForSpeech(Bundle bundle) {

            }

            @Override
            public void onBeginningOfSpeech() {

            }

            @Override
            public void onRmsChanged(float v) {

            }

            @Override
            public void onBufferReceived(byte[] bytes) {

            }

            @Override
            public void onEndOfSpeech() {

            }

            @Override
            public void onError(int i) {

            }

            @Override
            public void onResults(Bundle bundle) {

                ArrayList<String> matches=bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                if(matches!=null)
                {

                    comand=matches.get(0);


                    SetComandAction(comand,cntex);

                }
            }

            @Override
            public void onPartialResults(Bundle bundle) {

            }

            @Override
            public void onEvent(int i, Bundle bundle) {

            }
        });


        video=(VideoView) view.findViewById(R.id.videoView2);

        play=(Switch)view.findViewById(R.id.switch1);

        btn=(ImageButton) view.findViewById(R.id.button);





        btn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        record(recognizer,speechRecognizerIntent);
                    }
                }
        );




        ////////////////////////////////setting up slideshow//////

String command= getArguments().getString("attraction");

String path;

switch (command)
{


    case"Estadio Dragao":
        SelectedItemFragment.path=  setDragaoSlideshow(view);

        txt.setText("Have you ever thought of taking a seat on the 22nd chair of the Dragao Stadium's " +
                "Presidential Box? By visiting the Dragao Stadium, you will be able to experience the "
        +"President's Jorge Nuno Pinto da Costa place and enjoy an inside and privileged look " +
                "over this worldwide referenced work designed by architect Manuel Salgado.");
        break;//done

    case"Livraria Lelo":
        SelectedItemFragment.path=setLivaria(
                view);
        txt.setText("Livraria Lello is a bookshop with an extraordinary historical and architectural value " +
                "located at the number 144 of Rua das Carmelitas, in downtown Porto, in Portugal. Its  "
                +"origins date back to 1881, when the brothers José and António Lello opened an  " +
                "establishment at the nearby Rua do Almada, Porto, dedicated to publish and selling ");break;//done

    case"Porto Bridge":
        SelectedItemFragment.path= setPortoBridge(view);
        txt.setText("The only place in Europe where one can visit the arch of a bridge, experiencing an both  " +
                "iconic and impressive building for its size and elegance; it is the only national "
                +"monument of the twentieth century in Porto. ");break;


    case"Don Pedro":
        SelectedItemFragment.path= setPedro(view);
        txt.setText("Very accessible plaza, near many attractions (and food), visited by most tour groups  " +
                "but even with the crowds would be a good rendezvous spot if your group splits up and needs  "
                +"to reconvene later in the afternoon. Several excellent outdoor cafes line" +
                " this plaza so it's also good people watching spot.  ");break;


    case"Porto Cathedral":
        SelectedItemFragment.path=  setCathedralSlideshow(view);
        txt.setText("The Porto Cathedral (Portuguese: Sé do Porto) is a Roman Catholic church located in the historical centre of the city of Porto " +
                " Portugal. It is one of the city's oldest monuments and one of the most important local Romanesque monuments."
                +"The cathedral is flanked by two square towers, each supported with two buttresses and crowned with a cupola. " +
                "The façade shows a Baroque porch and a beautiful Romanesque rose window under a crenellated arch " +
                ", giving the impression of a fortified church.");break;


    case"Palacio da Bolsa":
        SelectedItemFragment.path= setPalazoSlideshow(view);
        txt.setText("National Monument, located in the historical centre of the City, classified as World " +
                "Heritage Site by UNESCO, the Palacio da Bolsa, built by the Porto Commercial."
                +"Asociation on the ruins of the Saint Francis Convent, has become by excellence the  " +
                "Porto city’s drawing room, welcoming the most illustrious visitors, amongst which are " +
                "Monarchs, Presidents and Ministers from almost every country.");break;


    case"Croft":
        SelectedItemFragment.path=  setCroft(view);
        txt.setText("What can you expect from visiting the oldest, continuously-running Port wine producer?  " +
                "The most basic answer is a sampling of three exceptional wines in addition to peering  "
                +"down the antique cellars. Croft also offers a broader tasting experience with more wine  " +
                "and accompaniments, in addition to blind tastings meant to enhance the senses.");break;
    case"Ferreira":
        SelectedItemFragment.path=  setFerreira(view);
        txt.setText("The Ferreira name is among the most popular for Port wine and can be found across  " +
                "Portugal as well as outside of the country. During a tour, visitors will sample aged varieties "
                +"(between two to five types, depending on the package) while learning about the Douro " +
                "region and the vineyard heiress who developed and gave Ferreira Port her name. ");break;


    case"Sandeman":
        SelectedItemFragment.path= setSanderman(view);
        txt.setText("What makes Sandeman wine different from Ferreira or any other Port? Learn during a visit  " +
                "in their Vila Nova da Gaia cellar. The classics include Ruby, Tawny, and White Port wine,  "
                +"and Sandeman also produces Brandy and Madeira Wine. Visits are also themed, and  " +
                "guests may be shown around by a man in a cape and sombrero.");break;

    default:break;






}

video.seekTo(100);

        play.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {


                        Uri uri=Uri.parse(SelectedItemFragment.path);
                        video.setVideoURI(uri);


                        if(b)
                        {

                            video.start();

                            //video.start();
                        }
                        else {video.pause();


                        }

                    }
                }
        );



/*if(command.equals("Porto Bridge"))setPortoBridge(view);
else setDragaoSlideshow(view);*/





        //////////////////////////////////////////////////////////////////////////////////////











        return view;

    }
    public void record(SpeechRecognizer rec,Intent i){


        rec.startListening(i);




    }

    public void checkPermission(){

        if(!(ContextCompat.checkSelfPermission(cntex, Manifest.permission.RECORD_AUDIO)== PackageManager.PERMISSION_GRANTED ))
        {
            Intent intent=new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:"+cntex.getPackageName()));

            startActivity(intent);
           getActivity().finish();
        }
    }

    public void SetComandAction(String s,Context context)
    {
        Intent f = new Intent();


        switch (s)
        {
            case"explore":

                f.setClass(cntex,Welcome.class);

                startActivity(f);
                break;

            case"menu":



                FragmentManager menu= getFragmentManager();
                FragmentTransaction menut=menu.beginTransaction();

                menut.replace(R.id.floadmenuFromComand,new menufragment()).commit();



                break;
            case"landmark":
                Intent itemSelect=new Intent();
                itemSelect.setClass(context,ItemSelctionActivity.class);
                itemSelect.putExtra("cardid","LandMarks");///extra information = Lanrmarks
                context.startActivity(itemSelect);
                break;
            case"historic":
                Intent historic=new Intent();
                historic.setClass(context,ItemSelctionActivity.class);
                historic.putExtra("cardid","HistoricSites");///extra information = Lanrmarks
                context.startActivity(historic);
                break;
            case"wine":
                Intent wine=new Intent();
                wine.setClass(context,ItemSelctionActivity.class);
                wine.putExtra("cardid","Wine");///extra information = Lanrmarks
                context.startActivity(wine);
                break;
            default:
                txt.setText("No comand given ");



        }




    }



    public String setDragaoSlideshow(View view)
    {


        stuff=new ArrayList<>();
        stuff.add(R.mipmap.image1);
        stuff.add(R.mipmap.image2);
        stuff.add(R.mipmap.image3);
        stuff.add(R.mipmap.image4);//done
        stuff.add(R.mipmap.image6);
        stuff.add(R.mipmap.image7);
        stuff.add(R.mipmap.image8);
        stuff.add(R.mipmap.image9);

        ViewPager viewPager= (ViewPager) view.findViewById(R.id.viewPager);
        Pageviewer_Swipe adapter=new Pageviewer_Swipe(stuff,getActivity());
        viewPager.setAdapter(adapter);
        String path="android.resource://"+cntex.getPackageName()+"/"+R.raw.dragao;
        return path;

    }



    public String setCathedralSlideshow(View view){
        stuff=new ArrayList<>();
        stuff.add(R.mipmap.cathedral1);
        stuff.add(R.mipmap.cathedral2);//done
        stuff.add(R.mipmap.cathedral3);
        stuff.add(R.mipmap.cathedral4);

        ViewPager viewPager= (ViewPager) view.findViewById(R.id.viewPager);
        Pageviewer_Swipe adapter=new Pageviewer_Swipe(stuff,getActivity());
        viewPager.setAdapter(adapter);
        String path="android.resource://"+cntex.getPackageName()+"/"+R.raw.cathedral;
        return path;
    }


    public String setPalazoSlideshow(View view){

        stuff=new ArrayList<>();
        stuff.add(R.mipmap.palazo1);
        stuff.add(R.mipmap.palazo2);
        stuff.add(R.mipmap.palazo3);
        stuff.add(R.mipmap.palazo4);///done

        ViewPager viewPager= (ViewPager) view.findViewById(R.id.viewPager);
        Pageviewer_Swipe adapter=new Pageviewer_Swipe(stuff,getActivity());
        viewPager.setAdapter(adapter);
        String path="android.resource://"+cntex.getPackageName()+"/"+R.raw.cathedral;
        return path;

    }
    public String setLivaria(View view){

        stuff=new ArrayList<>();
        stuff.add(R.mipmap.lelo1);
        stuff.add(R.mipmap.lelo2);//done
        stuff.add(R.mipmap.lelo3);
        stuff.add(R.mipmap.lelo4);
        stuff.add(R.mipmap.portomain);
        ViewPager viewPager= (ViewPager) view.findViewById(R.id.viewPager);
        Pageviewer_Swipe adapter=new Pageviewer_Swipe(stuff,getActivity());
        viewPager.setAdapter(adapter);
        String path="android.resource://"+cntex.getPackageName()+"/"+R.raw.levraria_lelo;
        return path;
    }
    public String setPortoBridge(View view){

        stuff=new ArrayList<>();
        stuff.add(R.mipmap.bridge1);
        stuff.add(R.mipmap.bridge2);//done
        stuff.add(R.mipmap.bridge3);
        stuff.add(R.mipmap.bridge4);

        ViewPager viewPager= (ViewPager) view.findViewById(R.id.viewPager);
        Pageviewer_Swipe adapter=new Pageviewer_Swipe(stuff,getActivity());
        viewPager.setAdapter(adapter);
        String path="android.resource://"+cntex.getPackageName()+"/"+R.raw.bridge_porto;
        return path;
    }
    public String setCroft(View view){

        stuff=new ArrayList<>();
        stuff.add(R.mipmap.croft1);
        stuff.add(R.mipmap.croft2);
        stuff.add(R.mipmap.croft3);//done
        stuff.add(R.mipmap.croft4);

        ViewPager viewPager= (ViewPager) view.findViewById(R.id.viewPager);
        Pageviewer_Swipe adapter=new Pageviewer_Swipe(stuff,getActivity());
        viewPager.setAdapter(adapter);
        String path="android.resource://"+cntex.getPackageName()+"/"+R.raw.wine_perreira;
        return path;
    }
    public String setFerreira(View view){

        stuff=new ArrayList<>();
        stuff.add(R.mipmap.f1);
        stuff.add(R.mipmap.f2);
        stuff.add(R.mipmap.f3);//done
        stuff.add(R.mipmap.f4);

        ViewPager viewPager= (ViewPager) view.findViewById(R.id.viewPager);
        Pageviewer_Swipe adapter=new Pageviewer_Swipe(stuff,getActivity());
        viewPager.setAdapter(adapter);
        String path="android.resource://"+cntex.getPackageName()+"/"+R.raw.wine_perreira;
        return path;
    }

    public String setSanderman(View view){

        stuff=new ArrayList<>();
        stuff.add(R.mipmap.s1);
        stuff.add(R.mipmap.s2);
        stuff.add(R.mipmap.s3);
        stuff.add(R.mipmap.s4);

        ViewPager viewPager= (ViewPager) view.findViewById(R.id.viewPager);
        Pageviewer_Swipe adapter=new Pageviewer_Swipe(stuff,getActivity());
        viewPager.setAdapter(adapter);
        String path="android.resource://"+cntex.getPackageName()+"/"+R.raw.wine_sanderman;
        return path;
    }
    public String setPedro(View view){

        stuff=new ArrayList<>();
        stuff.add(R.mipmap.pedro1);
        stuff.add(R.mipmap.pedro2);
        stuff.add(R.mipmap.pedro3);
        stuff.add(R.mipmap.pedro4);//done
        ViewPager viewPager= (ViewPager) view.findViewById(R.id.viewPager);
        Pageviewer_Swipe adapter=new Pageviewer_Swipe(stuff,getActivity());
        viewPager.setAdapter(adapter);
        String path="android.resource://"+cntex.getPackageName()+"/"+R.raw.portointro;
        return path;
    }
}