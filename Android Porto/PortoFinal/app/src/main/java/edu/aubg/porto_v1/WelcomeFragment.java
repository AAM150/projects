package edu.aubg.porto_v1;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.hitomi.cmlibrary.CircleMenu;
import com.hitomi.cmlibrary.OnMenuSelectedListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by amako on 2/19/2018.
 */

public class WelcomeFragment extends Fragment {


ImageView img;
Button btn;
String []names={"Menu",
                "Play",
                "Music"};
    List<Integer> stuff;
CircleMenu circleMenu;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
      View view=inflater.inflate(R.layout.portopicfragment,container,false  );
      circleMenu=(CircleMenu)view.findViewById(R.id.circlemenu);
      circleMenu.setMainMenu(Color.parseColor("#CDCDCD"),R.mipmap.button_home_white,R.drawable.x)
      .addSubMenu(Color.parseColor("#258CFF"),R.drawable.menu)
      .addSubMenu(Color.parseColor("#6d4c41"),R.drawable.play)
      .addSubMenu(Color.parseColor("#ff0000"),R.drawable.music)
      .setOnMenuSelectedListener(new OnMenuSelectedListener() {
          @Override
          public void onMenuSelected(int i) {


              if(names[i].equals("Menu"))
              {

                  loadmenu();

              }

              Toast.makeText(getActivity(),"You clicked "+names[i],Toast.LENGTH_SHORT).show();
          }
      })
        ;




      ;
        btn=(Button) view.findViewById(R.id.btn1);
///////////////////////////////upon click of the explore button event is fired///////////////////////////////////
        btn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                     //   loadmenu(view);//load menu is called
                    }
                }
        );



      return  view;
    }

    public void loadmenu()
    {

/////start transaction of fragment and supply the menu fragment in the same activity //
        FragmentManager menumanager= getFragmentManager();
        FragmentTransaction menuTransaction=menumanager.beginTransaction();

        menuTransaction.replace(R.id.fragmentholder,new menufragment()).commit();

    }
}
